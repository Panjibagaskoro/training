﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Training.API.Service;
using Training.Core.Model;

namespace Training.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class UploadBukuController : ControllerBase
    {
        private readonly IUploadBukuService _uploadBukuService;

        public UploadBukuController(IUploadBukuService uploadBukuService)
        {
            _uploadBukuService = uploadBukuService;
        }

        [HttpPost]
        public string[] UploadBuku (IFormFile files)
        {
            var msg = _uploadBukuService.UploadBuku(files);
            return msg;
        }
    }
}
