﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training.API.Service.Kategori;
using Training.Data.Entity;

namespace Training.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class KategoriController : ControllerBase
    {
        private readonly IKategoriService _kategoriService;

        public KategoriController(IKategoriService kategoriService)
        {
            _kategoriService = kategoriService;
        }

        [HttpGet]
        public List<Kategoris> GetKategori()
        {
            List<Kategoris> response = _kategoriService.GetKategori();

            return response;
        }
        
        [HttpGet("{id}")]
        public Kategoris GetKategoriById(int id)
        {
            Kategoris response = _kategoriService.GetKategoriById(id);

            return response;
        }
        
        [HttpPost]
        public Kategoris AddKategori(Kategoris kategoris)
        {
            Kategoris response = _kategoriService.AddKategori(kategoris);

            return response;
        }
        [HttpPut]
        public Kategoris UpdateKategori(int id)
        {
            Kategoris response = _kategoriService.UpdateKategori(id);

            return response;
        }

        [HttpDelete]
        public Kategoris DeleteKategori(int id)
        {
            Kategoris response = _kategoriService.DeleteKategori(id);

            return response;
        }
    }
}
