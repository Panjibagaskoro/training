﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training.API.Service.Home;
using Training.Core.Model;
using Training.Core.Model.HomeIndex;
using Training.Data.Entity;

namespace Training.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class HomeController : ControllerBase
    {
        private readonly IHomeService _homeService;

        public HomeController(IHomeService homeService)
        {
            _homeService = homeService;
        }


        //public IActionResult IndexHome()
        //{

        //    return View();
        //}

        [HttpGet]
        public List<Bukus> DaftarBukus()
        {
            List<Bukus> result = _homeService.DaftarBukus();

            return result;
        }

        [HttpGet("{category}")]
        public List<Bukus> GetBukuByCategory(string category)
        {
            List<Bukus> result = _homeService.GetBukuByCategory(category);

            return result;
        }

        [HttpPost("Cart")]
        public jumlahListBuku AddShopingCart([FromBody]Cart bukus)
        {
            jumlahListBuku result = _homeService.AddShopingCart(bukus);

            return result;
        }

        [HttpPost]
        public string[] SaveMyOrder([FromBody]string answer)
        {
            string[] result = _homeService.SaveMyOrder(answer);

            return result;
        }

        [HttpGet("history")]
        public string[] DisplayHistory()
        {
            string[] result = _homeService.DisplayHistory();

            return result;
        }

        [HttpGet("{id}")]
        public string[] DetailBuku(int id)
        {
            string[] result = _homeService.DetailBuku(id);

            return result;
        }


    }
}
