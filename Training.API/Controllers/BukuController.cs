﻿                                                                                                                                    using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Training.API.Service;
using Training.API.Service.BookService;
using Training.Data.Entity;

namespace Training.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class BukuController : ControllerBase
    {
        private readonly IBukuService _bukuservice;

        public BukuController(IBukuService bukuService)
        {
            _bukuservice = bukuService;
        }

        [HttpGet]
        public List<Bukus> GetBukus()
        {
            List<Bukus> response = _bukuservice.GetBukus();

            return response;
        }

        [HttpGet("{id}")]
        public Bukus GetBukusById(int id)
        {
            Bukus response = _bukuservice.GetBukusById(id);

            return response;
        }

        [HttpPost]
        public string[] PostBukus([FromBody] IFormFile file)
        {
            string[] response = _bukuservice.UploadBuku(file);

            return response;
        }

        [HttpGet("Edit/{id}")]
        public Bukus GetBukusEdit(int id)
        {
            Bukus response = _bukuservice.GetBukusEdit(id);

            return response;
        }

        [HttpPut]
        public string[] EditBuku([FromBody]IFormFile file)
        {
            string[] response = _bukuservice.EditBuku(file);

            return response;
        }
        
        [HttpDelete]
        public string[] DeleteBukus(string ServerFilePath)
        {
            string[] response = _bukuservice.DeleteBukus(ServerFilePath);

            return response;
        }

        //public string[] DaftarPeminjam()
        //{
        //    string[] response = _bukuservice.DaftarPeminjam();

        //    return response;
        //}

        [HttpGet("Detail/{id}")]
        public string[] DetailPeminjaman(int id)
        {
            string[] response = _bukuservice.DetailPeminjaman(id);

            return response;
        }

        [HttpPost]
        public string[] SaveDetailPeminjaman([FromBody]string answer)
        {
            string[] response = _bukuservice.SaveDetailPeminjaman(answer);

            return response;
        }


    }
}
