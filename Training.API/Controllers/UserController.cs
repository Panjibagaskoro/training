﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Training.API.Service.ClientUsers;
using Training.Data.Entity;

namespace Training.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IClientUsersService _userservice;

        public UserController(IClientUsersService userService)
        {
            _userservice = userService;
        }

        [HttpGet]
        public List<Users> GetUsers()
        {
            List<Users> response = _userservice.GetUsers();

            return response;
        }

        [HttpGet("{id}")]
        public Users GetUsersById(string id)
        {
            Users response = _userservice.GetUsersById(id);

            return response;
        }

        [HttpPost]
        public Users AddUsers([FromBody]Users users)
        {
            Users response = _userservice.AddUsers(users);

            return response;
        }

        [HttpPut]
        public Users UpdateEUsers(string id)
        {
            Users response = _userservice.UpdateUsers(id);

            return response;
        }

        [HttpDelete]
        public Users DeleteUsers(string id)
        {
            Users response = _userservice.DeleteUsers(id);

            return response;
        }
    }
}
