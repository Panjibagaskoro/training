﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Training.Data.Entity
{
    public partial class LatihanContext : DbContext
    {
        //public LatihanContext()
        //{
        //}

        public LatihanContext(DbContextOptions<LatihanContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Bukus> Bukus { get; set; }
        public virtual DbSet<Kategoris> Kategoris { get; set; }
        public virtual DbSet<OrderDetails> OrderDetails { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<UserCart> UserCart { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<__MigrationHistory> __MigrationHistory { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Bukus>(entity =>
            {
                entity.HasKey(e => e.BukuId)
                    .HasName("PK_dbo.Bukus");

                entity.HasIndex(e => e.KategoriId)
                    .HasName("IX_KategoriId");

                entity.HasOne(d => d.Kategori)
                    .WithMany(p => p.Bukus)
                    .HasForeignKey(d => d.KategoriId)
                    .HasConstraintName("FK_dbo.Bukus_dbo.Kategoris_KategoriId");
            });

            modelBuilder.Entity<Kategoris>(entity =>
            {
                entity.HasKey(e => e.KategoriId)
                    .HasName("PK_dbo.Kategoris");
            });

            modelBuilder.Entity<OrderDetails>(entity =>
            {
                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.HasOne(d => d.Buku)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.BukuId)
                    .HasConstraintName("FK_dbo.OrderDetails_dbo.Bukus_BukuId");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.OrderDetails)
                    .HasForeignKey(d => d.OrderId)
                    .HasConstraintName("FK_dbo.OrderDetails_dbo.Orders_OrderId");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.OrderId)
                    .HasName("PK_dbo.Orders");

                entity.Property(e => e.OrderId).ValueGeneratedNever();

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.Username)
                    .HasConstraintName("FK_dbo.Orders_dbo.Users_Username");
            });

            modelBuilder.Entity<UserCart>(entity =>
            {
                entity.Property(e => e.ID).ValueGeneratedNever();

                entity.Property(e => e.Status).IsUnicode(false);

                entity.Property(e => e.Username).IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.Username)
                    .HasName("PK_dbo.Users");
            });

            modelBuilder.Entity<__MigrationHistory>(entity =>
            {
                entity.HasKey(e => new { e.MigrationId, e.ContextKey })
                    .HasName("PK_dbo.__MigrationHistory");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
