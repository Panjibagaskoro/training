﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Training.Data.Entity
{
    public partial class Bukus
    {
        public Bukus()
        {
            OrderDetails = new HashSet<OrderDetails>();
        }

        [Key]
        public int BukuId { get; set; }
        [Required]
        public string Judul { get; set; }
        [Required]
        public string Penulis { get; set; }
        [Required]
        public string Penerbit { get; set; }
        public string Deskripsi { get; set; }
        public int KategoriId { get; set; }
        public bool Status { get; set; }
        public string Gambar { get; set; }

        [ForeignKey(nameof(KategoriId))]
        [InverseProperty(nameof(Kategoris.Bukus))]
        public virtual Kategoris Kategori { get; set; }
        [InverseProperty("Buku")]
        public virtual ICollection<OrderDetails> OrderDetails { get; set; }
    }
}
