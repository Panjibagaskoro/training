﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Training.Data.Entity
{
    public partial class OrderDetails
    {
        [Key]
        public Guid ID { get; set; }
        public Guid OrderId { get; set; }
        public int BukuId { get; set; }

        [ForeignKey(nameof(BukuId))]
        [InverseProperty(nameof(Bukus.OrderDetails))]
        public virtual Bukus Buku { get; set; }
        [ForeignKey(nameof(OrderId))]
        [InverseProperty(nameof(Orders.OrderDetails))]
        public virtual Orders Order { get; set; }
    }
}
