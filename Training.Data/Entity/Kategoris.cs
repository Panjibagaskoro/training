﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Training.Data.Entity
{
    public partial class Kategoris
    {
        public Kategoris()
        {
            Bukus = new HashSet<Bukus>();
        }

        [Key]
        public int KategoriId { get; set; }
        [Required]
        public string NamaKategori { get; set; }

        [InverseProperty("Kategori")]
        public virtual ICollection<Bukus> Bukus { get; set; }
    }
}
