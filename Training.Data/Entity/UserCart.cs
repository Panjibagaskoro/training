﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Training.Data.Entity
{
    public partial class UserCart
    {
        [Key]
        public Guid ID { get; set; }
        [Required]
        [StringLength(50)]
        public string Username { get; set; }
        public int BukuId { get; set; }
        public int KategoriId { get; set; }
        [Required]
        [StringLength(60)]
        public string Status { get; set; }
    }
}
