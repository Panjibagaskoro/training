﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Training.Data.Entity
{
    public partial class Users
    {
        public Users()
        {
            Orders = new HashSet<Orders>();
        }

        [Key]
        [StringLength(128)]
        public string Username { get; set; }
        [Required]
        public string Nama { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Role { get; set; }
        public bool Status { get; set; }

        [InverseProperty("UsernameNavigation")]
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
