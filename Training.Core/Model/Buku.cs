﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Training.Data.Entity;


namespace Training.Core.Model
{
    public class Buku
    {
        public int BukuId { get; set; }

        
        public string Judul { get; set; }

        
        public string Penulis { get; set; }

        
        public string Penerbit { get; set; }


        
        public string Deskripsi { get; set; }


        
        public int KategoriId { get; set; }

        
        public bool Status { get; set; }

        
        public string Gambar { get; set; }

        
        public virtual Kategori Kategori { get; set; }
    }

    public class Cart
    {
        public string Username { get; set; }
        public int BukuId { get; set; }
        public int KategoriId { get; set; }
        public string Status { get; set; }

    }
}
