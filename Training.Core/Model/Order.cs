﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Training.Data.Entity;

namespace Training.Core.Model
{
    public class Order
    {
        public int OrderId { get; set; }
       
        public DateTime OrderDate { get; set; }

        public DateTime? ReturnDate { get; set; }

        public string Username { get; set; }
        
        public virtual Users User { get; set; }

        public bool Closed { get; set; }

        public List<ODetails> OrderDetail { get; set; }
    }
        
}
