﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Training.Core.Model
{
    public class Home
    {
        public List<Buku> Bukus { get; set; }

        public List<Kategori> Kategoris { get; set; }
        public List<Order> Orders { get; set; }
        public List<ODetails> OrderDetails { get; set; }
        public List<User> Users { get; set; }
    }
}
