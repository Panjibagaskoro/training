﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Training.Core.Model.Book
{
    public class Book
    {
        public int Id { get; set; }

        public string Judul { get; set; }
        public string Penulis { get; set; }
        public string Penerbit { get; set; }
        public string Deskripsi { get; set; }
        public bool Status { get; set; }
        public bool IsAvailable { get; set; }
        public string Gambar { get; set; }
    }
}
