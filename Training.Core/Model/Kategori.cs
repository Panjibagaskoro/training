﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Training.Data.Entity;

namespace Training.Core.Model
{
    public class Kategori
    {
        public int KategoriId { get; set; }

      
        public string NamaKategori { get; set; }
        public List<Bukus> Buku { get; set; }
    }
}
