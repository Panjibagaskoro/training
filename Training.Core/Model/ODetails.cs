﻿using System;
using System.Collections.Generic;
using System.Text;
using Training.Data.Entity;

namespace Training.Core.Model
{
    public class ODetails
    {
        public int ID { get; set; }
        public int OrderId { get; set; }
        public int BukuId { get; set; }

        public virtual Buku Buku { get; set; }
        public virtual Order Order { get; set; }
    }
}
