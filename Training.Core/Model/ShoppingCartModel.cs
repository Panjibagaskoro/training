﻿using System;
using System.Collections.Generic;
using System.Text;
using Training.Data.Entity;

namespace Training.Core.Model
{
    public class ShoppingCartModel
    {
        public int ID { get; set; }
        public int OrderId { get; set; }
        public int BukuId { get; set; }
        public virtual Bukus Buku { get; set; }
        public string Username { get; set; }
        public string Judul { get; set; }
        public string Penulis { get; set; }
        public int JumlahBuku { get; set; }
    }

}
