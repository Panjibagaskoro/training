﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Training.Core.Model.Account
{
    public class ChangePasswordVM
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string message { get; set; }
    }
}
