﻿using System.ComponentModel.DataAnnotations;

namespace Training.Core.Model
{
    public class LoginViewModel
    {
        public bool AllowRememberLogin { get; set; }
        public string message { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

    }
}
