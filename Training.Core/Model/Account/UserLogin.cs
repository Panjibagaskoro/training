﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Training.Core.Model.Account
{
    public class UserLogin
    {
        public int token_expiry_in;
        public int refresh_token_expiry_in;

        public int Id { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public bool Status { get; set; }
        public List<string> Role { get; set; }
        public List<Order> Orders { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
