﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Training.Core.Model
{
    public class User
    {
        public string Username { get; set; }

        public string Nama { get; set; }

        public string Password { get; set; }

        public string Role { get; set; }

        public bool Status { get; set; }

        public List<Order> Orders { get; set; }
    }
}
