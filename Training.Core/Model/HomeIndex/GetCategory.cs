﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Training.Data.Entity;

namespace Training.Core.Model.HomeIndex
{
    public class GetCategory
    {
        public GetCategory()
        {
            Bukus = new HashSet<Bukus>();
        }

        public int KategoriId { get; set; }
        public string NamaKategori { get; set; }

        [InverseProperty("Kategori")]
        public virtual ICollection<Bukus> Bukus { get; set; }
    }
}
