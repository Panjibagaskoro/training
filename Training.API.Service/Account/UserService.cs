﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Training.Core.Model.Account;
using Training.Data.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace Training.API.Service.Account
{
    public interface IUserService
    {
        UserLogin GetByUserNameAsync(string Username);
        string[] GenerateUserToken(UserLogin model);
                
    }

    public class UserService : IUserService
    {
        private readonly LatihanContext db;
        private IConfiguration _config { get; }

        public UserService(LatihanContext context, IConfiguration config)
        {
            db = context;
            _config = config;
        }

        public UserLogin GetByUserNameAsync(string Username)
        {
            UserLogin user = new UserLogin();
            var data = (from a in db.Users
                        where a.Username == Username
                        select new UserLogin
                        {

                            FullName = a.Nama,
                            Password = a.Password,
                            Token = "",
                            UserName = a.Username,
                            Status = a.Status,
                        }).FirstOrDefault();
            if (data != null)
            {
                user = data;
            }
            return user;
        }

        public string[] GenerateUserToken(UserLogin model)
        {
            string[] msg = null;
            try
            {
                var checkuser = (from a in db.Users
                                 where a.Username.ToLower() == model.UserName.ToLower()
                                 select a).FirstOrDefault();
                if (checkuser != null)
                {
                    model.Token = GenerateJSONWebToken(model);
                    model.RefreshToken = Guid.NewGuid().ToString();
                    msg = new string[] { "OK" };
                }
                else
                {
                    msg = new string[] { "Error", "Invalid Credential!" };
                }
            }
            catch (Exception ex)
            {
                msg = new string[] { "Error", ex.Message };
            }
            return msg;
        }

        private string GenerateJSONWebToken(UserLogin userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(ClaimTypes.Sid, userInfo.Id.ToString()),
                new Claim(ClaimTypes.Name, userInfo.UserName ?? string.Empty),
                //new Claim(ClaimTypes.GivenName, userInfo.FirstName ?? string.Empty),
                //new Claim(ClaimTypes.Surname, userInfo.LastName ?? string.Empty),
                new Claim("UserName", userInfo.UserName ?? string.Empty),
                //new Claim("FullName", userInfo.FullName ?? string.Empty),
                new Claim("Password", userInfo.Password ?? string.Empty)
                //new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                  _config["Jwt:Issuer"],
                  claims,
                  expires: DateTime.Now.AddMinutes(720),
                  signingCredentials: credentials);


            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
