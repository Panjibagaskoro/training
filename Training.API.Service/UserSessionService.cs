﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Training.Data.Entity;

namespace Training.API.Service
{
    public interface IUserSessionService
    {
        int Id { get; }
        string FullName { get; }
        string UserName { get; }
        string Password { get; }
    }
    public class UserSessionService : IUserSessionService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UserSessionService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public int Id => Convert.ToInt32((_httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == ClaimTypes.Sid).FirstOrDefault()?.Value));

        public string UserName => _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault()?.Value;

        public string Password => _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "Password").FirstOrDefault()?.Value;

        public string FullName => _httpContextAccessor.HttpContext.User.Claims.Where(x => x.Type == "FullName").FirstOrDefault()?.Value;
    }
}
