﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Web.Mvc;
using Training.Data.Entity;

namespace Training.API.Service.Kategori
{
    public interface IKategoriService
    {
        List<Kategoris> GetKategori();
        Kategoris GetKategoriById(int id);
        Kategoris AddKategori(Kategoris kategoris);
        Kategoris UpdateKategori(int id);
        Kategoris DeleteKategori(int id);



    }
    public class KategoriService : IKategoriService
    {
        private readonly LatihanContext db;

        public KategoriService(LatihanContext context)
        {
            db = context;
        }

        public List<Kategoris> GetKategori()
        {
            var kategori = db.Kategoris.ToList();

            return kategori;
        }

        public Kategoris GetKategoriById(int id)
        {
            Kategoris kategoris = db.Kategoris.Find(id);

            return kategoris;
        }

        public Kategoris AddKategori(Kategoris kategoris)
        {
            if (kategoris != null)
            {
                db.Kategoris.Add(kategoris);
                db.SaveChanges();

                return kategoris;
            }

            return null;
        }

        public Kategoris UpdateKategori(int id)
        {
            var kategoris = db.Kategoris.Find(id);

            db.Entry(kategoris).State = EntityState.Modified;
            db.SaveChanges();

            return kategoris;
        }

        public Kategoris DeleteKategori(int id)
        {
            var kategoris = db.Kategoris.Find(id);
            db.Entry(kategoris).State = EntityState.Deleted;
            db.SaveChanges();

            return kategoris;
        }
    }
}
