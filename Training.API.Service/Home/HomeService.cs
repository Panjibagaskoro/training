﻿using System;
using System.Collections.Generic;
using System.Linq;
using Training.Core.Model;
using Training.Core.Model.HomeIndex;
using Training.Data.Entity;

namespace Training.API.Service.Home
{
    public interface IHomeService
    {
        List<Bukus> IndexHome();
        List<Bukus> DaftarBukus();
        List<Bukus> GetBukuByCategory(string category);
        jumlahListBuku AddShopingCart(Cart bukus);
        List<Bukus> DisplayMyOrder();
        //string[] SaveMyOrder(int[] bukuId);
        string[] SaveMyOrder(string answer);
        string[] DisplayHistory();
        string[] DetailBuku(int id);
        
    }
    public class HomeService : IHomeService 
    {
        private readonly LatihanContext db;
        private readonly IUserSessionService CurrentUser;
        public HomeService (LatihanContext context, IUserSessionService userSession)
        {
            db = context;
            CurrentUser = userSession;
        }

        public List<Bukus> IndexHome()
        {
            var model = db.Bukus.OrderBy(r => Guid.NewGuid()).Take(6).ToList();

            return model;
        }

        public List<Bukus> DaftarBukus()
        {
            var listBuku = (from b in db.Bukus
                            join c in db.Kategoris on b.KategoriId equals c.KategoriId
                            where Convert.ToBoolean(c.NamaKategori.ToLower().Equals("teknologi"))
                            select b).ToList();
            
            return  listBuku;
        }
                
        //GET
        public List<Bukus> GetBukuByCategory(string category)
        {
            var model = db.Bukus.
                Where(c => c.Kategori.NamaKategori.ToLower().Equals(category)).ToList();
            
            return model; 
        }

        //POST
        public jumlahListBuku AddShopingCart(Cart book)
        {
            int jumlahBuku = 0;

            var books = (from a in db.UserCart
                         join c in db.Bukus on a.BukuId equals c.BukuId
                         where a.Username == book.Username && !string.IsNullOrWhiteSpace(a.Status)
                         select new Bukus
                         {
                             BukuId = c.BukuId,
                             Judul = c.Judul,
                             Gambar = c.Gambar,
                             Penulis = c.Penulis
                         }).ToList();
            string messageUser = "";

            try
            {
                if (book != null)
                {
                    if (books != null)
                    {
                        var isDuplicate = books.Any(c => c.BukuId == book.BukuId);
                        
                        if (isDuplicate)
                        {
                            messageUser = "Buku telah ada didalam keranjang anda !";
                        }
                        else
                        {
                            UserCart dataBuku =  new UserCart()
                            {
                                Username = book.Username,
                                BukuId = book.BukuId,
                                KategoriId = book.KategoriId,
                                Status = book.Status
                            };
                            db.UserCart.Add(dataBuku);
                            messageUser = "Berhasil dimasukan kedalam keranjang";

                            jumlahBuku = books.Count + 1;
                        }
                    }
                    else
                    {
                        UserCart dataBuku = new UserCart()
                        {
                            Username = book.Username,
                            BukuId = book.BukuId,
                            KategoriId = book.KategoriId,
                            Status = book.Status
                        };
                        db.UserCart.Add(dataBuku);

                        messageUser = "Berhasil dimasukan kedalam keranjang";
                        jumlahBuku = 1;
                    }

                }
                else
                {
                    messageUser = "Coba kembali, terjadi kesalahan pada sistem !";
                }
            }
            catch (Exception ex)
            {
                messageUser = ex.Message;
            }
                                               
            jumlahListBuku data = new jumlahListBuku()
            {
                message = messageUser,
                jumlah = jumlahBuku
            };

            return data;
        }

        public List<Bukus> DisplayMyOrder() //get data
        {
            var curUsername = CurrentUser.UserName;
            var myOrder = (from a in db.Orders
                           join b in db.OrderDetails on a.OrderId equals b.OrderId
                           join c in db.Bukus on b.BukuId equals c.BukuId
                           where a.Username.ToLower().Equals(curUsername.ToLower())
                           select new Bukus
                           {
                               BukuId = b.BukuId,
                               Judul = c.Judul,
                               Penulis = c.Penulis,
                               Gambar = c.Gambar
                           }).ToList();

            return myOrder;
        }

        public string[] SaveMyOrder(string answer)
        {
            string[] msg = null;
            if (answer == "Save")
            {
                try
                {
                    var books = (from a in db.Orders
                                 join b in db.OrderDetails on a.OrderId equals b.OrderId
                                 join c in db.Bukus on b.BukuId equals c.BukuId
                                 select new Bukus
                                 {
                                     BukuId = b.BukuId,
                                     Judul = c.Judul,
                                     Penulis = c.Penulis,
                                     Gambar = c.Gambar
                                 }).ToList();
                    var details = books.Select(b => new ODetails()
                    {
                        BukuId = b.BukuId
                    }).ToList();

                    Guid guidHeader = Guid.NewGuid();
                    var order = new Orders()
                    {
                        OrderId = guidHeader,
                        OrderDate = DateTime.Now,
                        Username = CurrentUser.UserName

                    };

                    db.Orders.Add(order);
                    foreach (var buku in order.OrderDetails)
                    {
                        var obj = db.Bukus.First(b => b.BukuId == buku.BukuId);
                        obj.Status = false;
                        db.Update(obj);
                    }
                    db.SaveChanges();
                    msg = new string[] { "Success", "Data has been successfully saved" };
                }
                catch (Exception ex)
                {
                    msg = new string[] { "Error", "Unable to process", ex.Message };
                }                             

            }
            return msg;
        }


        //public string[] SaveMyOrder(int[] bukuId) //POST
        //{
        //    string[] msg = null;
        //    try
        //    {
        //        Guid guidHeader = Guid.NewGuid();
        //        var order = new Orders()
        //        {
        //            OrderId = guidHeader,
        //            OrderDate = DateTime.Now,
        //            Username = CurrentUser.UserName

        //        };

        //        db.Orders.Add(order);

        //        //List<Bukus> bukuList = SessionManager.Get<List<Bukus>>("ShoppingCart");
        //        List<Bukus> bukuList = (from a in db.Orders
        //                                join b in db.OrderDetails on a.OrderId equals b.OrderId
        //                                join c in db.Bukus on b.BukuId equals c.BukuId
        //                                where a.Username == "queen"
        //                                select new Bukus
        //                                {
        //                                    BukuId = c.BukuId,
        //                                    Judul = c.Judul,
        //                                    Gambar = c.Gambar,
        //                                    Penulis = c.Penulis
        //                                }).ToList();

        //        foreach (int i in bukuId)
        //        {
        //            Data.Entity.OrderDetails orderDetails = new Data.Entity.OrderDetails()
        //            {
        //                ID = Guid.NewGuid(),
        //                OrderId = guidHeader,
        //                BukuId = i
        //            };

        //            db.OrderDetails.Add(orderDetails);
        //        }



        //        //foreach (var bukus in bukuList)
        //        //{
        //        //    var bukuUpdate = db.Bukus.FirstOrDefault(b => b.BukuId == bukus.BukuId);
        //        //    bukuUpdate.Status = false;
        //        //}

        //        db.SaveChanges();
        //        msg = new string[] { "Success", "Data has been successfully saved" };
        //    }

        //    catch (Exception ex)
        //    {
        //        msg = new string[] { "Error", "Unable to process", ex.Message };

        //    }

        //    return msg;
        //}

        public string[] DisplayHistory()
        {
            string[] msg = null;
            try
            {
                var curUsername = CurrentUser.UserName;

                var historyList = (from od in db.OrderDetails
                                   join o in db.Orders on od.OrderId equals o.OrderId
                                   join b in db.Bukus on od.BukuId equals b.BukuId
                                   where o.Username.ToLower().Equals(curUsername.ToLower())
                                   select new HistoryVM()
                                   {
                                       BukuId = od.BukuId,
                                       Judul = b.Judul,
                                       Penulis = b.Penulis,
                                       Gambar = b.Gambar,
                                       Status = o.Closed ? "Sudah Dikembalikan " : "Masih Dipinjam",
                                       OrderDate = o.OrderDate,
                                       ReturnDate = o.ReturnDate
                                   }).ToList();
                
            }
            catch (Exception ex)
            {
                msg = new string[] { "Error", ex.Message };

            }
            return msg;
        }

        public string[] DetailBuku(int id)
        {
            string[] msg = null;
            try
            {
                var buku = db.Bukus.FirstOrDefault(b => b.BukuId.Equals(id));
                if (buku == null)
                {
                    msg = new string[] { "Error", "Buku tidak dapat ditampilkan" };
                }
                else
                {
                    return msg;
                }
            }
            catch (Exception ex)
            {
                msg = new string[] { "Error", "Unable to process", ex.Message };
            }
            return msg;
        }

    }
}
