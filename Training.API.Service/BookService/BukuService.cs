﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using Training.Core.Model;
using Training.Data.Entity;

namespace Training.API.Service.BookService
{
    public interface IBukuService
    {
        List<Bukus> GetBukus();
        Bukus GetBukusById(int id);
        string[] UploadBuku(IFormFile file);
        Bukus GetBukusEdit(int id);
        string[] EditBuku(IFormFile file);
        string[] DeleteBukus(string ServerFilePath);
        string[] DaftarPeminjam();
        string[] DetailPeminjaman(int id);
        string[] SaveDetailPeminjaman(string answer);        
    }

    public class BukuService : IBukuService
    {
        private readonly LatihanContext db;

        public BukuService(LatihanContext context)
        {
            db = context;
        }

        public List<Bukus> GetBukus() //GET BUKU INDEX
        {
            var bukus = db.Bukus.Include(b => b.Kategori);
            return (List<Bukus>)bukus;
        }

        public Bukus GetBukusById(int id) //GET BUKU DETAILS
        {
            Bukus bukus = db.Bukus.Find(id);

            return bukus;
        }

        public string[] UploadBuku(IFormFile file) //POST BUKU CREATE
        {
            string[] msg = null;
            try
            {
                if (file != null && !string.IsNullOrWhiteSpace(file.FileName))
                {
                    string FileName = Path.GetFileName(file.FileName);
                    string fileExt = Path.GetExtension(file.FileName).Substring(1).ToLower();
                    Guid guid = Guid.NewGuid();
                    string Id = guid.ToString();
                    string extentions = file.FileName.Substring(file.FileName.IndexOf('.'));
                    string ServerFileName = Id + extentions;
                    string ServerSavePath = Path.Combine(Directory.GetCurrentDirectory(), "Images", ServerFileName);
                    string ServerFilePath = ServerFileName;

                    var supportedTypes = new[] { "jpg", "jpeg", "png", "gif", "tif" };
                    if (supportedTypes.Contains(fileExt.ToLower()))
                    {


                        using (var img = Image.FromStream(file.OpenReadStream()))
                        {
                            int MaxWidth = 1024;
                            int MaxHeight = 768;
                            if (img.Width < 1024)
                            {
                                MaxWidth = img.Width;
                                MaxHeight = img.Width;
                            }
                            double ratioX = (double)MaxWidth / img.Width;
                            double ratioY = (double)MaxHeight / img.Height;
                            double ratio = Math.Min(ratioX, ratioY);
                            int newWidth = (int)(img.Width * ratio);
                            int newHeight = (int)(img.Height * ratio);

                            using (var output = File.Open(ServerSavePath, FileMode.Create))
                            {
                                Image thumbnail = img.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero);
                                thumbnail.Save(output, ImageFormat.Png);
                                img.Save(ServerSavePath);
                                //imageLogoPath = ServerFilePath;
                                msg = new string[] { "OK", ServerFilePath };
                            }

                        }
                    }
                }
                else
                {
                    msg = new string[] { "Error", "Not Uploaded" };
                }
            }
            catch (Exception ex)
            {
                msg = new string[] { "Error", ex.Message };
            }

            return msg;
        }

        public Bukus GetBukusEdit(int id) //GET BUKU EDIT
        {
            Bukus bukus = db.Bukus.Find(id);

            return bukus;
        }

        public string[] EditBuku(IFormFile file) //POST EDIT BUKU
        {
            string[] msg = null;
            try
            {

                if (file != null && !string.IsNullOrWhiteSpace(file.FileName))
                {
                    string FileName = Path.GetFileName(file.FileName);
                    string fileExt = Path.GetExtension(file.FileName).Substring(1).ToLower();
                    Guid guid = Guid.NewGuid();
                    string Id = guid.ToString();
                    string extentions = file.FileName.Substring(file.FileName.IndexOf('.'));
                    string ServerFileName = Id + extentions;
                    string ServerSavePath = Path.Combine(Directory.GetCurrentDirectory(), "Images", ServerFileName);
                    string ServerFilePath = ServerFileName;

                    var supportedTypes = new[] { "jpg", "jpeg", "png", "gif", "tif" };
                    if (supportedTypes.Contains(fileExt.ToLower()))
                    {


                        using (var img = Image.FromStream(file.OpenReadStream()))
                        {
                            int MaxWidth = 1024;
                            int MaxHeight = 768;
                            if (img.Width < 1024)
                            {
                                MaxWidth = img.Width;
                                MaxHeight = img.Width;
                            }
                            double ratioX = (double)MaxWidth / img.Width;
                            double ratioY = (double)MaxHeight / img.Height;
                            double ratio = Math.Min(ratioX, ratioY);
                            int newWidth = (int)(img.Width * ratio);
                            int newHeight = (int)(img.Height * ratio);

                            using (var output = File.Open(ServerSavePath, FileMode.Create))
                            {
                                Image thumbnail = img.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero);
                                thumbnail.Save(output, ImageFormat.Png);
                                img.Save(ServerSavePath);
                                //imageLogoPath = ServerFilePath;
                                msg = new string[] { "OK", ServerFilePath };
                            }

                        }
                    }
                }
                else
                {
                    msg = new string[] { "Error", "Not Uploaded" };
                }
            }
            catch (Exception ex)
            {
                msg = new string[] { "Error", ex.Message };
            }

            return msg;
        }

        public string[] DeleteBukus(string ServerFilePath) //DELETE BUKU
        {
            string[] msg = new string[] { "OK", "Book deleted" };
            if (!ServerFilePath.Contains("Images"))
            {
                Bukus bukus = db.Bukus.Find(ServerFilePath);
                if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), ServerFilePath)))
                {
                    File.Delete(Path.Combine(Directory.GetCurrentDirectory(), ServerFilePath));

                    msg = new string[] { "", "Book deleted" };
                }
                
                db.Bukus.Remove(bukus);
                db.SaveChanges();
            }
            return msg;
        }

        public string[] DaftarPeminjam() //get0
        {
            string[] msg = null;
            try
            {
                var peminjamList = (from o in db.Orders
                                    join u in db.Users on o.Username equals u.Username
                                    select new 
                                    {
                                        OrderId = o.OrderId,
                                        Peminjam = u.Nama,
                                        Status = o.Closed ? "Closed" : "Open",
                                        OrderDate = o.OrderDate,
                                        ReturnDate = o.ReturnDate
                                    }).ToList();
            }
            catch (Exception ex)
            {
                msg = new string[] { "Error", "Unable to Proccess.", ex.Message };
            }

            return msg;
        }

        public string[] DetailPeminjaman(int id) //GET
        {
            string[] msg = null;
            try
            {
                var detail = (from od in db.OrderDetails
                              join o in db.Orders on od.OrderId equals o.OrderId
                              join b in db.Bukus on od.BukuId equals b.BukuId
                              where o.OrderId.Equals(id)
                              select new
                              {
                                  OrderId = o.OrderId,
                                  BukuId = b.BukuId,
                                  Judul = b.Judul,
                                  Penulis = b.Penulis,
                                  Gambar = b.Gambar,
                                  Status = o.Closed
                              }).ToList();

                //SessionManager.Set("DetailPeminjaman", detail);
            }
            catch (Exception ex)
            {
                msg = new string[] { "Error", "Unable to Proccess.", ex.Message };
            }

            return msg;
        }

        public string[] SaveDetailPeminjaman(string answer)
        {
            string[] msg = null;
            if (answer == "Save")
            {
                try
                {
                    var details = new List<PeminjamanDetailVM>();//SessionManager.Get<List<PeminjamanDetailVM>>("DetailPeminjaman");
                    var id = details.First().OrderId;

                    var order = db.Orders.FirstOrDefault(c => c.OrderId.Equals(id));
                    order.Closed = true;
                    order.ReturnDate = DateTime.Now;
                    db.SaveChanges();

                    foreach (var buku in details)
                    {
                        var bukuUpdate = db.Bukus.FirstOrDefault(b => b.BukuId == buku.BukuId);
                        bukuUpdate.Status = true;
                    }

                    db.SaveChanges();
                    //Session.Remove("DetailPeminjaman");
                    msg = new string[] { "", "OK" };
                }

                catch (Exception ex)
                {
                    msg = new string[] { "Error", "Unable to Proccess.", ex.Message };
                }
            }
            return msg;
        }


    }
}
