﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Training.Data.Entity;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;

namespace Training.API.Service
{
    public interface IUploadBukuService
    {
        string[] UploadBuku(IFormFile file);
        //string[] DeleteBook(string ServerFilePath);

    }

    public class UploadBukuService : IUploadBukuService
    {
        private readonly LatihanContext db;

        public UploadBukuService(LatihanContext context)
        {
            db = context;
        }

       
        public string[] UploadBuku(IFormFile file)
        {
            string[] msg = null;
            try
            {
                if (file != null && !string.IsNullOrWhiteSpace(file.FileName))
                {
                    string FileName = Path.GetFileName(file.FileName);
                    string fileExt = Path.GetExtension(file.FileName).Substring(1).ToLower();
                    Guid guid = Guid.NewGuid();
                    string Id = guid.ToString();
                    string extentions = file.FileName.Substring(file.FileName.IndexOf('.'));
                    string ServerFileName = Id + extentions;
                    string ServerSavePath = Path.Combine(Directory.GetCurrentDirectory(), "Images",  ServerFileName);
                    string ServerFilePath = ServerFileName;

                    var supportedTypes = new[] { "jpg", "jpeg", "png", "gif", "tif" };
                    if (supportedTypes.Contains(fileExt.ToLower()))
                    {


                        using (var img = Image.FromStream(file.OpenReadStream()))
                        {
                            int MaxWidth = 1024;
                            int MaxHeight = 768;
                            if (img.Width < 1024)
                            {
                                MaxWidth = img.Width;
                                MaxHeight = img.Width;
                            }
                            double ratioX = (double)MaxWidth / img.Width;
                            double ratioY = (double)MaxHeight / img.Height;
                            double ratio = Math.Min(ratioX, ratioY);
                            int newWidth = (int)(img.Width * ratio);
                            int newHeight = (int)(img.Height * ratio);

                            using (var output = File.Open(ServerSavePath, FileMode.Create))
                            {
                                Image thumbnail = img.GetThumbnailImage(newWidth, newHeight, () => false, IntPtr.Zero);
                                thumbnail.Save(output, ImageFormat.Png);
                                img.Save(ServerSavePath);
                                //imageLogoPath = ServerFilePath;
                                msg = new string[] { "OK", ServerFilePath};
                            }

                        }
                    }
                }
                else
                {
                    msg = new string[] { "Error", "Not Uploaded" };
                }
            }
            catch (Exception ex)
            {
                msg = new string[] { "Error", ex.Message};
            }

            return msg;
        }

        //public string[] DeleteBook(string ServerFilePath)
        //{
        //    var msg = new string[] { "OK", "Book deleted" };
        //    if (!ServerFilePath.Contains("Images"))
        //    {
        //        if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), ServerFilePath)))
        //        {
        //            File.Delete(Path.Combine(Directory.GetCurrentDirectory(), ServerFilePath));

        //            msg = "Book deleted";
        //        }
        //    }
        //}


    }
}
