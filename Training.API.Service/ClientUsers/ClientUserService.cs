﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Training.Data.Entity;

namespace Training.API.Service.ClientUsers
{
    public interface IClientUsersService
    {
        List<Users> GetUsers();
        Users GetUsersById(string id);
        Users AddUsers(Users users);
        Users UpdateUsers(string id);
        Users DeleteUsers(string id);

    }

    public class ClientUserService : IClientUsersService
    {
        private readonly LatihanContext db;

        public ClientUserService (LatihanContext context)
        {
            db = context;
        }

        public List<Users> GetUsers()
        {            
            var user = db.Users.ToList();

            return user;
        }

        public Users GetUsersById(string id)
        {
            Users users = db.Users.Find(id);

            return users;
        }

        public Users AddUsers(Users users)
        {
            if (users != null)
            {
                db.Users.Add(users);
                db.SaveChanges();

                return users;
            }

            return null;
        }

        public Users UpdateUsers(string id)
        {
            var users = db.Users.Find(id);
            db.Update(users);
            db.Entry(users).State = EntityState.Modified;
            db.SaveChanges();

            return users;
        }

        public Users DeleteUsers(string id)
        {
            var users = db.Users.Find(id);
            db.Entry(users).State = EntityState.Deleted;
            db.SaveChanges();

            return users;
        }
    }

}
