﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Training.API.Service.Account;
using Training.API.Service.BookService;
using Training.API.Service.ClientUsers;
using Training.API.Service.Home;
using Training.API.Service.Kategori;

namespace Training.API.Service
{
    public static class ConfigureDependencyInjection
    {
        public static IServiceCollection ConfigureLibraryDependencyInjection(this IServiceCollection services)
        {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserSessionService, UserSessionService>();
            services.AddScoped<IHomeService, HomeService>();
            services.AddScoped<IBukuService, BukuService>();
            services.AddScoped<IClientUsersService, ClientUserService>();
            services.AddScoped<IKategoriService, KategoriService>();
            services.AddScoped<IUploadBukuService, UploadBukuService>();
            return services;
        }
    }
}
