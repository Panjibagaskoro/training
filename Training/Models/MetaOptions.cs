﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Training.Models
{
    public class MetaOptions
    {
        public string Version { get; set; }
        public string Protocol { get; set; }
        public string API_Base_URL { get; set; }
        public string WEB_URL { get; set; }
        public string PathBase { get; set; }
    }
}
