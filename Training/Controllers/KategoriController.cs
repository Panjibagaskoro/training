﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Training.Controllers
{
    public class KategoriController : Controller
    {
        public IActionResult IndexKategori()
        {
            return View();
        }

        public IActionResult Detailskategori()
        {
            return View();
        }

        public IActionResult EditKategori()
        {
            return View();
        }

        public IActionResult DeleteKategori()
        {
            return View();
        }

        public IActionResult TambahKategori()
        {
            return View();
        }
    }
}
