﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Training.API.Service;
using Training.API.Service.Account;
using Training.Core.Model;
using Training.Core.Model.Account;
using Training.Data.Entity;

namespace Training.Controllers
{
    public class AccountController : Controller
    {
        private IConfiguration Config { get; }
        private readonly IUserService _userService;

        public AccountController(IConfiguration config
            , IUserService userService)
        {
            Config = config;
            _userService = userService;
        }

        //public IActionResult LoginIndex()
        //{
        //    return View();
        //}

        [AllowAnonymous]
        [HttpGet]
        public IActionResult LoginIndex(string ReturnUrl)
        {
            LoginViewModel view = new LoginViewModel
            {
                AllowRememberLogin = true
            };
            return View(view);

        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> LoginIndex(LoginViewModel model, string button)
        {
            LoginViewModel view = new LoginViewModel();
            if (button != "login")
            {
                view.message = "Invalid Request!";
                view.AllowRememberLogin = true;
                return View(view);
            }
            else
            {
                if (model != null)
                {
                    var username = model.Username;
                    var user = _userService.GetByUserNameAsync(username);
                    if (user != null)
                    {
                        if (user.Status == false)
                        {
                            view.message = "Your Account is not register!";
                            view.AllowRememberLogin = true;
                            return View(view);
                        }
                        else
                        {
                           // var generate_token = await _userService.GenerateJSONWebToken(user);
                            var generate_token = GenerateJSONWebToken(user);
                            
                            user.Token = generate_token;
                            HttpContext.Session.SetString("Bearer", user.Token);
                            HttpContext.Request.Headers.Add("Bearer", user.Token);
                            view.message = "Invalid Username or Password!";
                            view.AllowRememberLogin = true;
                        }
                        return Redirect("~/");
                    }
                    else
                    {
                        view.message = "Invalid Username or Password!";
                        view.AllowRememberLogin = true;
                        return View(view);
                    }
                }
                return View();
            }

        }

        private string GenerateJSONWebToken(UserLogin userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(ClaimTypes.Sid, userInfo.Id.ToString()),
                new Claim(ClaimTypes.Name, userInfo.UserName ?? string.Empty),
                new Claim("FullName", userInfo.FullName ?? string.Empty),
                new Claim("Password", userInfo.Password ?? string.Empty)
                //new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(Config["Jwt:Issuer"],
              Config["Jwt:Issuer"],
              claims,
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Logout()
        {
            //input logic for logout
            HttpContext.Session.Clear();
            return Redirect("LoginIndex");
            
        }

        [Authorize]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangePassword(ChangePasswordVM model)
        {
            ChangePasswordVM view = new ChangePasswordVM();
            try
            {
                if (model != null)
                {
                    //var curUsername = SessionManager.FindByName(model.UserName);
                    //var curUsername = CurrentUser.UserName; //SessionManager.Get<string>("Username");
                    //var user = db.Users.FirstOrDefault(u =>
                    //    u.Username.ToLower().Equals(curUsername.ToLower()));
                    //if (user != null)
                    //{
                    //    if (user.Password == model.CurrentPassword)
                    //    {
                    //        user.Password = model.NewPassword;
                    //        db.SaveChanges();
                    //        view.message = "Password berhasil diganti!";
                    //    }
                    //    else
                    //    {
                    //        view.message = "Password lama salah!";
                    //    }
                    //}
                    //else
                    //{
                    //    view.message = "Username tidak dikenal!";
                    //}
                }
            }
            catch (Exception ex)
            {
                view.message = "";
            }
            return View();
        }
    }

}

    
