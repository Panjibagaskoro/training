﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training.API.Service.Home;
using Training.Data.Entity;

namespace Training.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        public IActionResult IndexHome()
        {
            return View();
        }

        public IActionResult DaftarBukus()
        {
            return View();
        }

        public IActionResult DisplayMyOrder()
        {
            return View();
        }

        public IActionResult DisplayHistory()
        {
            return View();
        }

        public IActionResult DetailBuku()
        {
            return View();
        }
     
    }
}
