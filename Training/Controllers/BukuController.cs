﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Training.Data.Entity;
using Training.API.Service;

namespace Training.Controllers
{
    [Authorize]
    public class BukuController : Controller
    {
        public IActionResult IndexBukus()
        {
            return View();
        }

        public IActionResult DetailsBuku()
        {
            return View();
        }

        public IActionResult TambahBuku()
        {
            return View();
        }

        public IActionResult DeleteBuku()
        {
            return View();
        }

        public IActionResult EditBuku()
        { 
            return View();
        }

        public IActionResult DaftarPeminjam()
        {
            return View();
        }

        public IActionResult DetailPeminjaman()
        {
            return View();
        }
    }
}
